# Tooling of KWinFT's Tooling

## Documentation
Uses the commit message lint functionality from this repo.
See there for information on how the commitlint tool can be run locally too.

[docs]: https://gitlab.com/kwinft/tooling/-/tree/master/docs
